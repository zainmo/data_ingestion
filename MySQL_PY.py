
# coding: utf-8

# In[2]:


#Modules
from lxml import etree
import json
import MySQLdb
import time
import pandas as pd
import re

#Files
xml_data = open('productdata_29.xml', 'r')

#Lists
final_list = []
mapped_nodes_keys = []
product_id_list = []
test_row = []
product_id = []
query_dataframe = []
category_query = []
category_query_output = []
tags_list_to_set = []

#Dictionaries
single_record = {}
mapped_nodes = {}
bidding_information = {}
test_record = {}
query_result = {}
ox_campaigns = {}

#Counters
break_counter = 0
file_counter = 0

#Variables


# In[3]:


from sqlalchemy import *
from sqlalchemy.exc import OperationalError


class sqlA:
    def __init__(self, config):
        try:
            self.conn_string = self.generate_conn_string(config)
            self.engine = create_engine(self.conn_string, echo=True, pool_recycle=25)
            self.connection = self.engine.connect()
        except Exception as e:
            print(str(e))

    def generate_conn_string(self, config):
        """"
        mysql+mysqldb://scott:tiger@localhost/test?charset=utf8&use_unicode=0
        """
        return ''.join([config['type'], r'://', config['user'], r':', config['passwd'], r'@', config['host'], r':', str(config['port']), r'/', config['db'], r'?', 'charset=', config['charset'], '&use_unicode=0'])

    def query(self, query, params=None):
        result = []
        try:
            result = self.connection.execute(text(query), params) if params is not None else self.connection.execute(text(query))
        except Exception as e:
            if isinstance(e, OperationalError):
                print('Connection timed out. Retrying...')
                result = self.connection.execute(text(query), params) if params is not None else self.connection.execute(text(query))
            else:
                print(str(e))
        return result
    
sqlA = sqlA({'charset': 'utf8',
'db': 'openx',
'host': 'localhost',
'passwd': 'root',
'port': 3306,
'type': 'mysql+mysqldb',
'use_unicode': True,
'user': 'root'})


# In[4]:


def fetch_bidding_information(product_id_list):
    bidding_information = sqlA.query("SELECT campaignid, productid, max_cpc, max_cpm, max_ecpa FROM bidding_data WHERE `productid` in :plist", {"plist":product_id_list})
    
    for info in bidding_information:
        query_result = {}
        query_result['cpc'] = info['max_cpc'].decode("utf-8")
        query_result['cpm'] = info['max_cpm'].decode("utf-8")
        query_result['cps'] = info['max_ecpa'].decode("utf-8")
        query_result['product_id'] = info['productid'].decode("utf-8")
        query_result['campaign_id'] = info['campaignid'].decode("utf-8")
        
        query_dataframe.append(query_result)
    


# In[5]:


def fetch_category_information(array):
    record_counter = len(array)
    while record_counter_0 > 0:
        temp_key = ''
        for i in array:
            for j in i:
                if 'category' in j:
                    temp_key = temp_key + 'category'
                else:
                    temp_key = temp_key + '#'
                if 'sub_category' in j:
                    temp_key = temp_key + '-sub_category'
                else:
                    temp_key = temp_key + '-#'
                if 'sub_sub_category' in j:
                    temp_key = temp_key + '-sub_sub_category'
                else:
                    temp_key = temp_key + '-#'
                if 'category_level_4' in j:
                    temp_key = temp_key + '-category_level_4'
                else:
                    temp_key = temp_key + '-#'
                if 'category_level_5' in j:
                    temp_key = temp_key + '-category_level_5'
                else:
                    temp_key = temp_key + '-#'
        category_query.append(temp_key)
        record_counter_0 = record_counter_0 - 1
    category_information = sqlA.query("SELECT `tyroo_affinity`, `advertiser_category_map` FROM     category_scheme_advertiser WHERE `advertiser_category_map` in :clist", {"clist"=category_query})
    for info in category_information:
        category_query_output.append(info.decode("utf-8"))
    for i in array:
        for j in i:
            for x in category_query_output:
                for y in x:
                    if x == j:
                        i['tyroo_affinity_category'] = x['tyroo_affinity']


# In[ ]:


cur = sqlA.query("SELECT * FROM ox_parent_rss WHERE id = 695")
for i in cur:
    campaign_id = i['campaign_id']
    
cur = sqlA.query("SELECT * FROM ox_mapped_nodes WHERE parent_rss_id = 695")
#Mapping nodes to a dictionary.
for row in cur.fetchall():
    #print(row)
    mapped_nodes[row[3].decode("utf-8")] = row[2].decode("utf-8")
    

#Creating a list of the keys of mapped_nodes.
for key in mapped_nodes.keys():
    mapped_nodes_keys.append(key)

cur = sqlA.query("SELECT * FROM `ox_campaigns` WHERE `campaignid` = 18440")
for i in cur:
    
    client_id = i['clientid']


# In[ ]:


#Parsing the XML using iterparse().
for event, element in etree.iterparse('productdata_29.xml', encoding='utf-8',recover = True, events=('start', 'end', 'start-ns', 'end-ns')):
    
    for i in element:
        tags_list_to_set.append(i.tag)
    tag_set = set(tags_list_to_set)
    
    

    #timeout = time.time() + 5
    if element.tag == 'record' and event == 'start':
 
        single_record = {}
        for i in mapped_nodes_keys:
            try:
                temp_key = mapped_nodes[element.find(i).tag]
                single_record[temp_key] = str(element.find(i).text)
    
            except Exception:
                pass
        try:
            if 'product_id' and 'product_url' and 'image_url1' in single_record:
                temp_product_id = single_record['product_id']
                single_record['_id'] = str(temp_product_id) + '_' + str(campaign_id)
                product_id_list.append(single_record['product_id'])
        
                final_list.append(single_record)
                break_counter += 1
            else:
                element.clear()
        except Exception:
            pass
        
        element.clear()
    

    #Flushing records stored in final_list if 10000 records have been processed.
    if break_counter == 10:
        fetch_bidding_information(product_id_list)
        final_list_db = pd.DataFrame(final_list)
        query_dataframe_db = pd.DataFrame(query_dataframe)
        frames = [final_list_db, query_dataframe_db]
        result = pd.concat(frames)
        output = result.to_json(orient='record')
        record_list = open('test_folder/record_list_'+str(file_counter)+'.json', 'w')
        record_list.write(str(output))
        file_counter += 1
        record_list.close()
        break_counter = 0
        print('A file has been written!')

        final_list = []
    #Flushing records stored in final_list if all records have been processed,
    #and there are fewer than 10000 records in the list. (To prevent a loop)
    #elif time.time() > timeout:
        #record_list = open('test_folder/record_list_'+str(file_counter)+'.json', 'w')
        #record_list.write(str(json.dumps(final_list)))
        #file_counter += 1
        #record_list.close()
        #break_counter = 0
        #print('The final file has been written.')
        #exit()


# In[ ]:




